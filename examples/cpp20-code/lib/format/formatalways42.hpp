//********************************************************
// The following code example is taken from the book
//  C++20 - The Complete Guide
//  by Nicolai M. Josuttis (www.josuttis.com)
//  http://www.cppstd20.com
//
// The code is licensed under a
//  Creative Commons Attribution 4.0 International License
//  http://creativecommons.org/licenses/by/4.0/
//********************************************************


#include "always42.hpp"
#include <format>

template<>
struct std::formatter<Always42> : std::formatter<std::string>
{
  auto format(const Always42& value, std::format_context& ctx)
  {
    // convert to string:
    std::string s = std::to_string(value.getValue());
    // and use standard string formatter:
    return std::formatter<std::string>::format(s, ctx);
  }
};

