//********************************************************
// The following code example is taken from the book
//  C++20 - The Complete Guide
//  by Nicolai M. Josuttis (www.josuttis.com)
//  http://www.cppstd20.com
//
// The code is licensed under a
//  Creative Commons Attribution 4.0 International License
//  http://creativecommons.org/licenses/by/4.0/
//********************************************************


#include "always40.hpp"
#include <format>
#include <iostream>

template<>
struct std::formatter<Always40>
{
  // parse the format string for this type:
  auto parse(std::format_parse_context& ctx) -> decltype(ctx.begin()) {
    return ctx.begin();    // return position of }
  }

  // format by always writing its value:
  auto format(const Always40& value, std::format_context& ctx)
  {
    std::format_to(ctx.out(), "{}", value.getValue());
    return ctx.out();
  }
};

