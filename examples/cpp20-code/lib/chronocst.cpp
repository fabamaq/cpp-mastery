//********************************************************
// The following code example is taken from the book
//  C++20 - The Complete Guide
//  by Nicolai M. Josuttis (www.josuttis.com)
//  http://www.cppstd20.com
//
// The code is licensed under a
//  Creative Commons Attribution 4.0 International License
//  http://creativecommons.org/licenses/by/4.0/
//********************************************************


#include <iostream>
#include <chrono>

int main()
{
  auto abbrev = "CST";
  auto day = std::chrono::sys_days{2021_y/1/1};
  auto& db = std::chrono::get_tzdb();

  // print time and name of all time zones with abbrev:
  std::cout << abbrev << " at "
            << std::chrono::zoned_time{"UTC", day} << ":\n";
  for (const auto& z : db.zones) {
    if (z.get_info(day).abbrev == abbrev) {
      std::chrono::zoned_time zt{&z, day};
      std::cout << "  " << zt << "  " << z.name() << '\n';
    }
  }
}

