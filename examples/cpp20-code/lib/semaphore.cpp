//********************************************************
// The following code example is taken from the book
//  C++20 - The Complete Guide
//  by Nicolai M. Josuttis (www.josuttis.com)
//  http://www.cppstd20.com
//
// The code is licensed under a
//  Creative Commons Attribution 4.0 International License
//  http://creativecommons.org/licenses/by/4.0/
//********************************************************


#include <iostream>
#include <queue>
#include <chrono>
#include <thread>
#include <mutex>
#include <semaphore>
using namespace std::literals;  // for duration literals

int main()
{
  std::queue<char> values;  // queue of values
  std::mutex valuesMx;      // mutex to protect access to the queue

  // initialize a queue with some values
  // - no mutex because no other thread is running yet:
  for (int i = 0; i < 1000; ++i) {
    values.push(char(i % (128-32) + 32));
  }
  
  // create a pool of numThreads threads:
  // - limit their availability with a semaphore (initially none available):
  constexpr int numThreads = 10;
  std::counting_semaphore<numThreads> enabled{0};

  // create and start all threads of the pool:
  std::vector<std::jthread> pool;
  for (int idx = 0; idx < numThreads; ++idx) {
    pool.push_back(std::jthread{[&, idx] (std::stop_token st) {
                                  while (!st.stop_requested()) {
                                    // request to become one of the enabled threads:
                                    enabled.acquire();

                                    // get next value from the queue:
                                    char val;
                                    {
                                      std::lock_guard lg{valuesMx};
                                      val = values.front();
                                      values.pop();
                                    }
                                    // print the value 10 times:
                                    for (int i = 0; i < 10; ++i) {
                                      std::cout.put(val).flush();
                                      auto dur = 200ms * ((idx % 3) + 1);
                                      std::this_thread::sleep_for(dur);
                                    }

                                    // release from the set of enabled threads:
                                    enabled.release();
                                  }
                                }});                       
  }

  // enable and disable threads in the thread pool:
  std::cout << "== wait 2 seconds (no thread enabled)" << std::endl;  
  std::this_thread::sleep_for(2s);

  std::cout << "== enable 3 parallel threads" << std::endl;  
  enabled.release(3);
  std::this_thread::sleep_for(2s);

  std::cout << "\n== enable 2 more parallel threads" << std::endl;  
  enabled.release(2); 
  std::this_thread::sleep_for(2s);

  std::quick_exit(0);
}

