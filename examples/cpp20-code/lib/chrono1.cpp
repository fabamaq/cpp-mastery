//********************************************************
// The following code example is taken from the book
//  C++20 - The Complete Guide
//  by Nicolai M. Josuttis (www.josuttis.com)
//  http://www.cppstd20.com
//
// The code is licensed under a
//  Creative Commons Attribution 4.0 International License
//  http://creativecommons.org/licenses/by/4.0/
//********************************************************


#include <chrono>
#include <iostream>

int main()
{
  namespace chr = std::chrono;      // shortcut for std::chrono
  using namespace std::literals;    // for h, min, y suffixes

  // for each 5th of all months of 2021:
  chr::year_month_day first = 2021y / 1 / 5;
  for (auto d = first; d.year() == first.year(); d += chr::months{1}) {
    // print out the date:
    std::cout << d << '\n';

    // init and print 18:30 UTC of those days:
    auto tp{chr::sys_days{d} + 18h + 30min};                // system/UTC time
    std::cout << "  UTC:    " << tp << '\n';

    // print date/time with specific time zones:
    chr::zoned_time timeBerlin{"Europe/Berlin", tp};        // Berlin time
    chr::zoned_time timeUsWest{"America/Los_Angeles", tp};  // L.A. time
    std::cout << "  Berlin: " << timeBerlin << '\n';
    std::cout << "  L.A.:   " << timeUsWest << '\n';
  }
}

