//********************************************************
// The following code example is taken from the book
//  C++20 - The Complete Guide
//  by Nicolai M. Josuttis (www.josuttis.com)
//  http://www.cppstd20.com
//
// The code is licensed under a
//  Creative Commons Attribution 4.0 International License
//  http://creativecommons.org/licenses/by/4.0/
//********************************************************


#include <iostream>
#include <cmath>

template<auto GetVat>
int addTax(int value)
{
  return static_cast<int>(std::round(value * (1 + GetVat()))); 
}

auto getTax = [] {
  return 0.19;
};

int main()
{
  std::cout << addTax<getTax>(100) << '\n';
  std::cout << addTax<getTax>(4199) << '\n';
  std::cout << addTax<getTax>(1950) << '\n';
}

