//********************************************************
// The following code example is taken from the book
//  C++20 - The Complete Guide
//  by Nicolai M. Josuttis (www.josuttis.com)
//  http://www.cppstd20.com
//
// The code is licensed under a
//  Creative Commons Attribution 4.0 International License
//  http://creativecommons.org/licenses/by/4.0/
//********************************************************


module;              // start module unit with global module fragment

#include <iostream>
#include <string>
#include <vector>

export module Mod1;  // module declaration

struct Order {
  int count;
  std::string name;
  double price;

  Order(int c, std::string n, double p)
   : count{c}, name{n}, price{p} {
  }
};

export class Customer {
 private:
  std::string name;
  std::vector<Order> orders;
 public:
  Customer(std::string n)
   : name{n} {
  }
  void buy(std::string ordername, double price) {
    orders.push_back(Order{1, ordername, price});
  }
  void buy(int num, std::string ordername, double price) {
    orders.push_back(Order{num, ordername, price});
  }
  double averagePrice() const;
  double sumPrice() const;
  void print() const;
};

