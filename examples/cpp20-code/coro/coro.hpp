//********************************************************
// The following code example is taken from the book
//  C++20 - The Complete Guide
//  by Nicolai M. Josuttis (www.josuttis.com)
//  http://www.cppstd20.com
//
// The code is licensed under a
//  Creative Commons Attribution 4.0 International License
//  http://creativecommons.org/licenses/by/4.0/
//********************************************************


#include <iostream>
#include <coroutine>      // for std::suspend_always()
#include "corotask.hpp"   // for CoroTask

CoroTask sayHello()
{
  std::cout << "Hello" << '\n';
  co_await std::suspend_always();       // SUSPEND
  std::cout << "World" << '\n';
  co_await std::suspend_always();       // SUSPEND
  std::cout << "!" << '\n';
}

