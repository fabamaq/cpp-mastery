//********************************************************
// The following code example is taken from the book
//  C++20 - The Complete Guide
//  by Nicolai M. Josuttis (www.josuttis.com)
//  http://www.cppstd20.com
//
// The code is licensed under a
//  Creative Commons Attribution 4.0 International License
//  http://creativecommons.org/licenses/by/4.0/
//********************************************************


#include <iostream>
#include <thread>
#include <chrono>
#include "coro.hpp"

int main()
{
  using namespace std::literals;

  // start coroutine:
  CoroTask sayHelloTask = sayHello();
  std::cout << "coroutine sayHello() started\n";

  // loop to resume the coroutine until it is done:
  while (sayHelloTask.resume()) {        // resume
    std::this_thread::sleep_for(500ms);
  }
}

