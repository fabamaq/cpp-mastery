//********************************************************
// The following code example is taken from the book
//  C++20 - The Complete Guide
//  by Nicolai M. Josuttis (www.josuttis.com)
//  http://www.cppstd20.com
//
// The code is licensed under a
//  Creative Commons Attribution 4.0 International License
//  http://creativecommons.org/licenses/by/4.0/
//********************************************************


#include <iostream>
#include <thread>
#include <chrono>
#include <coroutine>        // for std::suspend_always()
#include "stringtask.hpp"   // for StringTask

StringTask computeInSteps()
{
  std::string ret;
  ret += "Hello";
  co_await std::suspend_always();        // SUSPEND
  ret += " World";
  co_await std::suspend_always();        // SUSPEND
  ret += "!";
  co_return ret;
}

int main()
{
  using namespace std::literals;

  // start coroutine:
  StringTask task = computeInSteps();

  // loop to resume the coroutine until it is done:
  while (task.resume()) {                // RESUME
    std::this_thread::sleep_for(500ms);
  }

  // print return value of coroutine:
  std::cout << "result: " << task.getResult() << '\n';
}

