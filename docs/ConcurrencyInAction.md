# Concurrency In Action

## Chapter 1. Hello, world of concurrency in C++!

...

### 1.1. What is concurrency?

...

### 1.2. Why use concurrency?

...

### 1.3. Concurrency and multithreading in C++

...

### 1.4. Getting started

...

### Summary

...

## Chapter 2. Managing threads

...

### 2.1. Basic thread management

...

### 2.2. Passing arguments to a thread function

...

### 2.3. Transferring ownership of a thread

...

### 2.4. Choosing the number of threads at runtime

...

### 2.5. Identifying threads

...

### Summary

...

## Chapter 3. Sharing data between threads

...

### 3.1. Problems with sharing data between threads

...

### 3.2. Protecting shared data with mutexes

...

### 3.3. Alternative facilities for protecting shared data

...

### Summary

...

## Chapter 4. Synchronizing concurrent operations

...

### 4.1. Waiting for an event or other condition

...

### 4.2. Waiting for one-off events with futures

...

### 4.3. Waiting with a time limit

...

### 4.4. Using synchronization of operations to simplify code

...

### Summary

...

## Chapter 5. The C++ memory model and operations on atomic types

...

### 5.1. Memory model basics

...

### 5.2. Atomic operations and types in C++

...

### 5.3. Synchronizing operations and enforcing ordering

...

### Summary

...

## Chapter 6. Designing lock-based concurrent data structures

...

### 6.1. What does it mean to design for concurrency?

...

### 6.2. Lock-based concurrent data structures

...

### 6.3. Designing more complex lock-based data structures

...

### Summary

...

## Chapter 7. Designing lock-free concurrent data structures

...

### 7.1. Definitions and consequences

...

### 7.2. Examples of lock-free data structures

...

### 7.3. Guidelines for writing lock-free data structures

...

### Summary

...

## Chapter 8. Designing concurrent code

...

### 8.1. Techniques for dividing work between threads

...

### 8.2. Factors affecting the performance of concurrent code

...

### 8.3. Designing data structures for multithreaded performance

...

### 8.4. Additional considerations when designing for concurrency

...

### 8.5. Designing concurrent code in practice

...

### Summary

...

## Chapter 9. Advanced thread management

...

### 9.1. Thread pools

...

### 9.2. Interrupting threads

...

### Summary

...

## Chapter 10. Parallel algorithms

...

### 10.1. Parallelizing the standard library algorithms

...

### 10.2. Execution policies

...

### 10.3. The parallel algorithms from the C++ Standard Library

...

### Summary

...

## Chapter 11. Testing and debugging multithreaded applications

...

### 11.1. Types of concurrency-related bugs

...

### 11.2. Techniques for locating concurrency-related bugs

...

### Summary

...

