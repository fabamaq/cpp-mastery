#pragma once

/// @brief A simple message queue

#include <condition_variable>
#include <memory>
#include <mutex>
#include <queue>

namespace messaging {

	/// @brief base class of your queue entries
	struct message_base {
		virtual ~message_base() {}
	};

	/// @note each message type has a specialization
	template <typename Msg> struct wrapped_message : message_base {
		Msg contents;
		explicit wrapped_message(Msg const &contents_) : contents(contents_) {}
	};

	/// @brief message queue
	class queue {
		std::mutex m;
		std::condition_variable c;
		/// @note internal queue stores pointers to message_base
		std::queue<std::shared_ptr<message_base>> q;

	  public:
		template <typename T> void push(T const &msg) {
			std::lock_guard<std::mutex> lk(m);
			/// @note wrap posted message and store pointer
			q.push(std::make_shared<wrapped_message<T>>(msg));
			c.notify_all();
		}

		std::shared_ptr<message_base> wait_and_pop() {
			std::unique_lock<std::mutex> lk(m);
			/// @note block until queue isn't empty
			c.wait(lk, [&] { return !q.empty(); });
			auto res = q.front();
			q.pop();
			return res;
		}
	};

} // namespace messaging
