#pragma once

/// @brief The sender class

#include "queue.hpp"

namespace messaging {

	class sender {
		/// @note sender is a wrapper around que queue pointer
		queue *q;

	  public:
		/// @note default constructed sender has no queue
		sender() : q(nullptr) {}

		/// @note allow construction from pointer to queue
		explicit sender(queue *q_) : q(q_) {}

		template <typename Message> void send(Message const &msg) {
			if (q) {
				/// @note sending pushes message to queue
				q->push(msg);
			}
		}
	};

} // namespace messaging
