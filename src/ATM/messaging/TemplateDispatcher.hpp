#pragma once

/// @brief the TemplateDispatcher class template

#include "queue.hpp"

namespace messaging {

	template <typename PreviousDispatcher, typename Msg, typename Func>
	class TemplateDispatcher {
		queue *q;
		PreviousDispatcher *prev;
		Func f;
		bool chained;

		TemplateDispatcher(TemplateDispatcher const &) = delete;
		TemplateDispatcher &operator=(TemplateDispatcher const &) = delete;

		/// @note TemplateDispatcher instantiations are friends of each other
		template <typename Dispatcher, typename OtherMsg, typename OtherFunc>
		friend class TemplateDispatcher;

		void wait_and_dispatch() {
			for (;;) {
				auto msg = q->wait_and_pop();
				/// @note if you handle the message, break out of the loop
				if (dispatch(msg))
					break;
			}
		}

		bool dispatch(std::shared_ptr<message_base> const &msg) {
			/// @note check the message type and call the function
			if (wrapped_message<Msg> *wrapper =
					dynamic_cast<wrapped_message<Msg> *>(msg.get())) {
				f(wrapper->contents);
				return true;
			} else {
				/// @note chain to the previous dispatcher
				return prev->dispatch(msg);
			}
		}

	  public:
		TemplateDispatcher(TemplateDispatcher &&other)
			: q(other.q), prev(other.prev), f(std::move(other.f)),
			  chained(other.chained) {
			other.chained = true;
		}

		TemplateDispatcher(queue *q_, PreviousDispatcher *prev_, Func &&f_)
			: q(q_), prev(prev_), f(std::forward<Func>(f_)), chained(false) {
			prev_->chained = true;
		}

		/// @note additional handlers can be chained
		template <typename OtherMsg, typename OtherFunc>
		TemplateDispatcher<TemplateDispatcher, OtherMsg, OtherFunc>
		handle(OtherFunc &&of) {
			return TemplateDispatcher<TemplateDispatcher, OtherMsg, OtherFunc>(
				q, this, std::forward<OtherFunc>(of));
		}

		/// @note the destructor might throw exceptions
		~TemplateDispatcher() noexcept(false) {
			if (!chained) {
				wait_and_dispatch();
			}
		}
	};

} // namespace messaging
