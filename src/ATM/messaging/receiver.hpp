#pragma once

/// @brief the receiver class

#include "dispatcher.hpp"
#include "queue.hpp"
#include "sender.hpp"

namespace messaging {

	class receiver {
		/// @note the receiver owns the queue
		queue q;

	  public:
		/// @note allow implicit conversion to a sender that references the
		/// queue
		operator sender() { return sender(&q); }

		/// @note waiting for a queue creates a dispatcher
		dispatcher wait() { return dispatcher(&q); }
	};

} // namespace messaging
