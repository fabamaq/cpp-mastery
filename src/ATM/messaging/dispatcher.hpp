#pragma once

/// @brief the dispatcher class

#include "TemplateDispatcher.hpp"
#include "queue.hpp"

#include <memory>

namespace messaging {

	/// @brief the message for closing the queue
	class close_queue {};

	class dispatcher {
		queue *q;
		bool chained;

		/// @note dispatcher instances cannot be copied
		dispatcher(dispatcher const &) = delete;

		dispatcher &operator=(dispatcher const &) = delete;

		/// @note allow TemplateDispatcher instances to access the internals
		template <typename Dispatcher, typename Msg, typename Func>
		friend class TemplateDispatcher;

		void wait_and_dispatch() {
			/// @note loop, waiting for, and dispatching messages
			for (;;) {
				auto msg = q->wait_and_pop();
				dispatch(msg);
			}
		}

		/// @note dispatch() checks for a close_queue message, and throws
		bool dispatch(std::shared_ptr<message_base> const &msg) {
			if (dynamic_cast<wrapped_message<close_queue> *>(msg.get())) {
				throw close_queue();
			}
			return false;
		}

	  public:
		/// @note dispatcher instances can be moved
		dispatcher(dispatcher &&other) : q(other.q), chained(other.chained) {
			/// @note the source shouldn't wait for messages
			other.chained = true;
		}

		explicit dispatcher(queue *q_) : q(q_), chained(false) {}

		/// @brief handle a specific type of message with a TemplateDispatcher
		template <typename Message, typename Func>
		TemplateDispatcher<dispatcher, Message, Func> handle(Func &&f) {
			return TemplateDispatcher<dispatcher, Message, Func>(
				q, this, std::forward<Func>(f));
		}

		/// @note the destructor might throw exceptions
		~dispatcher() noexcept(false) {
			if (!chained) {
				wait_and_dispatch();
			}
		}
	};

} // namespace messaging
