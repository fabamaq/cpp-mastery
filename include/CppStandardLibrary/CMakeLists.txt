# =============================================================================
# creates an Object library to pass-down to other cmake targets

add_library(global_pre_compiled_headers OBJECT)

target_sources(
  global_pre_compiled_headers
  PUBLIC ${ROOT_DIR}/include/CppStandardLibrary/StandardLibrary.cpp
)
target_link_libraries(
  global_pre_compiled_headers PUBLIC project_global_settings
)
