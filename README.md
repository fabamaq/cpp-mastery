# C++ Mastery KS

A C++ Mastery Knowledge Sharing experience.

---
## DISCLAIMER
> This project is under constructions.
---

## Download

Download the zip archive and extract the files or clone the project:

```bash
  git clone --recurse-submodules git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-mastery.git
```

### Build Requirements

* [g++](https://gcc.gnu.org/) (10.3 or higher)
* [cmake](https://cmake.org/) (3.20.0 or higher)
* * [make](https://www.gnu.org/software/make/) (4.2.1 or higher)

### Build Process

With `cmake`

```bash
# From the root directory

# Generate the build system
# run cmake . -B <build_directory> -D CMAKE_BUILD_TYPE=[Debug|Release]
# Example
cmake . -B build/Debug/ -D CMAKE_BUILD_TYPE=Debug

# Build the project
# run cmake --build <build_directory> --target <TARGET>
# Example
cmake --build/Debug --target atm_example
```

Or you can setup your Integrated Development Environment (IDE) with cmake but, instructions for that, are outside the scope of this README (which can be updated with images if you want).

## Main Projects

If using `cmake`, all projects include a pre_compiled_headers library of the C++ Standard Library and a project_global_settings Interface Target to be linked to individual projects. 

### ATM example

The message-passing framework and complete ATM example from C++ Concurrency in Action.

## Examples Projects

The examples folder contains code from experts in the area of Concurrency and Networking, as well as C++20 sample code.

### **C++ Concurrency in Action**

* C++ Concurrency in Action is a book from Anthony Williams that teaches everything about elegant multithreaded applications in C++17.
* You can buy the book at [Amazon](https://www.amazon.com/C-Concurrency-Action-Anthony-Williams-ebook/dp/B0977ZDXX5).
* You can find the code [here](https://github.com/anthonywilliams/ccia_code_samples).

### **C++20 The Complete Guide**

* C++20 The Complete Guide is a book from Nicolai M. Josuttis that describes the latest evolution in modern C++ programming.
* You can find the draft of the book at  [here](http://www.cppstd20.com/).
* You can find the code [here](http://www.cppstd20.com/).

### **CppCon**

* CppCon is one of the biggest C++ Conferences in the world that provides invaluable information and techniques for C++.
* You can find the YouTube Channel [here](https://www.youtube.com/user/CppCon).
* You can find the presentations [here](https://github.com/CppCon).

#### **NetworkingTS**

* The code and talks in the CppCon folder are related to the Networking TS (Technical Specification).
* * The Networking TS is based on boost::asio (as most of the new features in c++).
* * The Networking TS will be included in C++ 23 after the approval of the Concurrency TS.

### **plf**

* PLF is a group of libraries developed by Matt Bentley to be integrated in C++26 or C++29 (depends on the roadmap)
* * In particular, `std::hive` is a new data structure very common in Game Engines and Game Developement.
* You can find the code [here](https://github.com/mattreecebentley)

## External Projects

At the moment, the external folder contains the Google Test Framework only, which is added to cmake automatically.

## Authors

- [Pedro André Oliveira](https://www.linkedin.com/in/pedroandreoliveira/)

## Contributing

Contributions are always welcome!

## Related Knowledge Sharings

* [C++ Test-Driven Development](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-test-driven-development.git)
* [C++ Design Patterns KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-design-patterns.git)
* [C++ Clean Architecture KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-clean-architecture.git)

* [C++ Fundamentals KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-fundamentals.git)
* [C++ Intermediate KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-intermediate.git)
* [C++ Advanced KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-advanced.git)
* [C++ Mastery KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-mastery.git)

## References

* [C++ High Performance](https://www.amazon.com/High-Performance-Master-optimizing-functioning/dp/1839216549)
* [The Art of Writing Efficient Programs](https://www.amazon.com/gp/product/B09BZTGJM2)
* [Asio C++](https://think-async.com/Asio/)
* [Boost::Asio](https://www.boost.org/doc/libs/1_77_0/doc/html/boost_asio.html)

### YouTube

* MIT OpenCourseWare on Performance Engineering of Software Systems
* * [Multicore Programming](https://www.youtube.com/watch?v=dx98pqJvZVk)
* * [Races and Parallelism](https://www.youtube.com/watch?v=a_R_DpsENfk)
* * [Analysis of Multithreaded Algorithms](https://www.youtube.com/watch?v=6I26_r1BKd8)
* * and many more

* CppCon on Networking TS
* * [The Networking TS in Practice: Testable, Composable Asynchronous I/O in C++](https://www.youtube.com/watch?v=hdRpCo94_C4)
* * [The Networking TS in Practice: Patterns for Real World Problems](https://www.youtube.com/watch?v=3wy1OPqNZZ8)
* * [The Networking TS from Scratch: I/O Objects](https://www.youtube.com/watch?v=xgXFZ-rYc4w)

* Asio
* * [Talking Async Ep1: Why C++20 is the Awesomest Language for Network Programming](https://www.youtube.com/watch?v=icgnqFM-aY4&)
* * * [github](https://github.com/chriskohlhoff/talking-async)

* CppCon on Concurrency
* * too many!!! just search "CppCon Concurrency" and start with the earliest videos
